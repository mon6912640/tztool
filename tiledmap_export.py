"""
    这个脚本用于将TiledMap的tmx文件中的tileset标签替换成tsx文件中的tileset标签
    就是地编里的Embed Tileset功能
    因为项目里动态加载没有加载tsx文件，所以需要把tsx文件的内容嵌入到tmx文件中
"""
import argparse
import shutil
import xml.etree.ElementTree as ET
from pathlib import Path


def run(p_soure, p_out):
    # 读取目录下所有的tmx文件
    list_file = p_soure.rglob('*.tmx')
    image_set = set()
    tmx_cnt = 0
    for f in list_file:
        # print(f)
        tree = ET.parse(f)
        root = tree.getroot()
        root.set('version', '1.0')
        parent_map = {c: p for p in tree.iter() for c in p}
        tilesets = tree.findall('tileset')
        # print(tilesets)
        for v in tilesets:
            # 读取tileset标签的firstgid属性
            firstgid_val = v.attrib['firstgid']
            # print(firstgid_val)

            # 读取对应的tsx文件
            tsx_file = f.parent / Path(v.attrib['source'])
            # print(tsx_file.exists())
            tsx_tree = ET.parse(tsx_file)
            tsx_tileset = tsx_tree.getroot()

            # 读取tsx文件的image属性
            tsx_image = tsx_tileset.find('image')
            # print(tsx_image.attrib['source'])
            image_path = f.parent / Path(tsx_image.attrib['source'])
            image_set.add(str(image_path))

            # print(tsx_tileset)
            # 修改tsx文件的firstgid属性
            tsx_tileset.set('firstgid', firstgid_val)
            # print(tsx_tileset.attrib['firstgid'])

            parent = parent_map.get(v)
            v_index = -1
            if parent is not None:
                v_index = list(parent).index(v)

            # 把tmx文件的tileset标签替换成tsx文件的tileset标签，需要顶替原来的位置
            if v_index >= 0:
                parent.insert(v_index, tsx_tileset)
                parent.remove(v)  # 删除tmx文件的tileset标签

        tree.write(p_out / f.name, encoding='UTF-8', xml_declaration=True)
        tmx_cnt += 1
        # break
    # 复制image_set中的图片到out目录
    img_cnt = 0
    for img in image_set:
        img_path = Path(img)
        img_out = p_out / img_path.name
        img_out.parent.mkdir(parents=True, exist_ok=True)
        # 复制图片文件到out目录，使用shutil模块
        shutil.copy(img_path, img_out)
        img_cnt += 1

    print(f'...导出了{tmx_cnt}个tmx文件，{img_cnt}个图片文件')
    print(f'...输出目录：{p_out.absolute()}')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='帮助信息')
    parser.add_argument('--source', type=str, default='', help='tmx源目录')
    parser.add_argument('--out', type=str, default='', help='tmx输出目录')

    args = parser.parse_args()

    if args.source != '':
        source = Path(args.source)
    else:
        source = Path('./tiledmap/map_source')

    if args.out != '':
        out = Path(args.out)
    else:
        out = Path('./tiledmap/map_out')

    run(source, out)
