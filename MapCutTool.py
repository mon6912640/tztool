import os
from PIL import Image


def split_image(image_path, grid_size, output_dir, quality=95):
    # 确保输出目录存在
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # 打开图片
    image = Image.open(image_path)

    # 获取图片尺寸
    width, height = image.size

    # 计算可以切割出的行数和列数
    num_rows = height // grid_size
    num_cols = width // grid_size

    # 切割并保存每个小图
    for row in range(num_rows):
        for col in range(num_cols):
            # 计算当前小图的位置和大小
            left = col * grid_size
            upper = row * grid_size
            right = (col + 1) * grid_size
            lower = (row + 1) * grid_size

            # 切割图片
            tile = image.crop((left, upper, right, lower))

            # 保存小图，使用 x_y 的命名格式，并指定输出目录和质量
            tile_name = f"{col}_{row}.jpg"
            tile_path = os.path.join(output_dir, tile_name)
            tile.save(tile_path, 'JPEG', quality=quality)
            print(f"Saved {tile_path} with quality {quality}")


# 使用方法
image_path = "map/map1.jpg"  # 替换为你的地图图片路径
grid_size = 512  # 设置你想要的格子大小
output_dir = "map_out"  # 指定输出目录
quality = 80  # 指定JPEG图像质量，默认是95

split_image(image_path, grid_size, output_dir, quality)
