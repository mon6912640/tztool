import argparse
import json
import re
import sys
from pathlib import Path
from typing import List

import cchardet as chardet
from openpyxl import Workbook

key_map = {}
find_cnt = 0


class VoFind:
    file_path: Path = None
    start_index = 0
    end_index = 0
    find_str = ''
    # 0 Lan.str  1 Lan.rep
    type_str = ''
    is_special = False

    # 引号
    quotation = None

    out_str = ''

    # 流水id
    uid = 0

    def __init__(self):
        global find_cnt
        find_cnt += 1
        self.uid = find_cnt

    def get_key(self):
        key = ''
        strip_str = self.find_str.lstrip().rstrip()  # 去除左右两边的空白字符
        reg_str = '{0}(.+?)(?<!\\\){0}'.format(self.quotation)
        # print('reg_str={0}'.format(reg_str))
        rec = re.compile(r'' + reg_str + '')  # 根据引号不同动态编译正则表达式
        # objtest = rec.search(strip_str)
        # if objtest:
        #     print('objtest.group(1)={0}'.format(objtest.group(1)))
        if self.out_str:  # 先处理替换修改过的，这里已经变成rep的形式
            # obj = rec.search(self.out_str)
            obj = re.search(r'\"(.+?)(?<!\\)\",', self.out_str)  # 这里不用原来的引号来匹配，需要匹配双引号
        elif self.type_str == 'rep':
            obj = rec.search(strip_str)
            # obj = re.search(r'[\'\"`](.+)[\'\"`],', strip_str)
        elif self.type_str == 'str':
            obj = rec.search(strip_str)
            # obj = re.search(r'[\'\"`](.+)[\'\"`]', strip_str)
        else:
            obj = None
        if obj:
            key = obj.group(1)
            if '\\n' in key:
                key = key.replace('\\n', '\n')
            if '\\t' in key:
                key = key.replace('\\t', '\t')
            key_map[key] = ''
        # print('-------- strip_str={0}, key={1}'.format(strip_str, key))


class VoTs:
    url = ''
    file_path: Path = None
    code_str = ''
    has_special = False
    list_find: List[VoFind] = None

    def __init__(self):
        self.list_find = []


def get_encoding(p_file):
    with open(p_file, 'rb') as f:
        msg = f.read()
        result = chardet.detect(msg)['encoding']
        return result


def run(p_work, p_output, p_others=None):
    list_file = []
    if p_others:
        for v in p_others:
            psrc = Path(v)
            if psrc.exists():
                list_file.extend(psrc.rglob('*.ts'))
    path_src = Path(p_work)
    list_file.extend(path_src.rglob('*.ts'))
    list_file.sort()
    ts_count = len(list_file)
    print('共有{0}个ts文件'.format(ts_count))

    # 含有Lan函数的文件数量
    has_lan_ts_cnt = 0

    encoding_set = set()
    modify_ts_list = []  # 需要把str包含${x}写法的替换成rep的ts文件列表
    for v in list_file:
        if v.name == 'Lan.ts':
            continue
        if '.d' in v.stem:  # 排除.d.ts文件
            continue
        # print(v.stem)
        # test
        # if v.name != 'HashObject.ts':
        #     continue
        cur_encoding = get_encoding(str(v))
        file_str = v.read_text(encoding=cur_encoding)
        result = find_lan(file_str, 0)
        end_index = result[0]
        if end_index < 0:
            continue
        vots = VoTs()
        vots.url = str(v)
        vots.file_path = v
        vots.code_str = file_str
        len_str = len(file_str)
        pos = 0
        # print(v)
        # print(result)
        while end_index > -1:
            # print(v, file_str[end_index], end_index)
            quotation = result[3]  # 引号字符串
            bk = False
            # 匹配函数内容
            for i in range(end_index, len_str):
                if file_str[i] == '(' and bk is False:
                    bk = True
                elif file_str[i] == ')' and bk is True:
                    bk = False
                elif file_str[i] == ')' and bk is False:
                    vof = VoFind()
                    vof.file_path = v
                    vof.find_str = file_str[end_index:i]
                    vof.type_str = result[1]
                    vof.start_index = end_index
                    vof.end_index = i
                    vof.quotation = quotation
                    vots.list_find.append(vof)
                    pos = i

                    inner_find_result = find_lan(vof.find_str, 0)
                    # print('inner_find_result', inner_find_result)
                    if inner_find_result[0] > -1:  # Lan函数内还有嵌套的Lan函数
                        pos = end_index + inner_find_result[2]  # inner_find_result[2]是Lan函数内的Lan函数的起始位置
                        # print('inner pos', pos)

                    # 把${xxx}替换成rep那样字符串+数组的形式
                    if vof.type_str == 'str' and quotation == '`' and '${' in vof.find_str:
                        vof.is_special = True
                        vots.has_special = True
                        params = []

                        def rpl_func(ma):
                            s2 = ma.group(2)
                            params.append(s2)
                            return '{' + str(len(params) - 1) + '}'

                        strip_str = vof.find_str.lstrip().rstrip()  # 去除左右两边的空字符
                        out_str = re.sub(r'(\$){([^}]+)}', rpl_func, strip_str, flags=re.M)
                        out_str = out_str + ', [{0}]'.format(', '.join(params))
                        out_str = out_str.replace('`', '"')
                        # print(v)
                        # print(vof.find_str)
                        # print('params', params)
                        # print('outstr', out_str)
                        vof.out_str = out_str

                    vof.get_key()  # 提取key
                    break
            # print('pos', pos)
            result = find_lan(file_str, pos)
            end_index = result[0]
        if vots.has_special:
            modify_ts_list.append(vots)

        # 查找是否有Lan.list
        find_list_flag = find_lan_list(file_str)

        # 查找是否有Lan.map
        find_map_flag = find_lan_map(file_str)

        if len(vots.list_find) > 0 or find_list_flag or find_map_flag:
            has_lan_ts_cnt += 1
        encoding_set.add(cur_encoding)
    # print(encoding_set)
    print('    其中有{0}个ts文件含有Lan函数'.format(has_lan_ts_cnt))
    for vts in modify_ts_list:
        for vfind in vts.list_find:
            if vfind.is_special:
                # 把 str(`xxxx${xxxx}`替换成rep("xxxx{0}", [xxxx]
                old_str = vfind.type_str + '(' + vfind.find_str
                new_str = 'rep(' + vfind.out_str
                # print('-- id: {0}'.format(vfind.uid))
                # print('    {0} 替换为 {1}'.format(old_str, new_str))
                vts.code_str = vts.code_str.replace(old_str, new_str)
        vts.file_path.write_text(vts.code_str, encoding='utf-8')
    json_str = json.dumps(key_map, indent=4, ensure_ascii=False)
    # print(json_str)
    path_json = Path(p_output, 'lan_cn.json')
    # print(path_json)
    path_json.parent.mkdir(parents=True, exist_ok=True)
    path_json.write_text(json_str, encoding='utf-8')
    print('...成功生成语言配置 {0}'.format(path_json))


def find_lan(p_str, p_pos):
    """
    查找Lan.str( 或是 Lan.rep(
    :param p_str:
    :param p_pos:
    :return:
        0 end_index 匹配到的模式的结束位置（减去空白字符和引号的长度）,
        1 type_str 匹配到的是str还是rep,
        2 start_index 匹配到的模式的开始位置,
        3 quotation 匹配到的引号类型（反引号、单引号或双引号）,
        4 old_end_index 匹配到的模式的结束位置
    """
    # 查找出Lan.str( 或是 Lan.rep(
    pattern = re.compile(r'\WLan\.(str|rep)\((\s*)([`\'\"])')
    start_index = -1
    quotation = None
    old_end_index = -1
    end_index = -1
    type_str = ''
    obj = pattern.search(p_str, p_pos)
    if obj:
        span = obj.span()
        type_str = obj.group(1)
        whitespace = obj.group(2)
        quotation = obj.group(3)  # 引号
        # print('find_lan', type_str, whitespace)
        len_whitespace = 0
        if whitespace:  # 有空白字符
            len_whitespace = len(whitespace)
        end_index = span[1] - 1 - len_whitespace  # 减去空白字符和引号的长度
        old_end_index = span[1]
        start_index = span[0]  # 增加起始位置
        # print('find_lan', span)
    return [end_index, type_str, start_index, quotation, old_end_index]


def find_lan_list(p_str):
    """
    查找Lan.list
    :param p_str:
    :return:
    """
    find_flag = False
    pattern = re.compile(r'\WLan\.list\(\s*\[(.+?)]\s*\)', flags=re.M | re.S)
    finds = pattern.finditer(p_str)
    for m in finds:
        find_flag = True
        # print(m.group(1))
        p2 = re.compile(r'[\'"](.*?)[\'"]')
        keys = p2.findall(m.group(1))
        for key in keys:
            if '\\n' in key:
                key = key.replace('\\n', '\n')
            if '\\t' in key:
                key = key.replace('\\t', '\t')
            key_map[key] = ''
    return find_flag


def find_lan_map(p_str):
    """
    查找Lan.map
    :param p_str:
    :return:
    """
    find_flag = False
    pattern = re.compile(r'\WLan\.map\(\s*\{(.+?)}\s*\)', flags=re.M | re.S)
    finds = pattern.finditer(p_str)
    for m in finds:
        find_flag = True
        # print(m.group(1))
        p2 = re.compile(r':\s*[\'"](.*?)[\'"]')
        keys = p2.findall(m.group(1))
        for key in keys:
            if '\\n' in key:
                key = key.replace('\\n', '\n')
            if '\\t' in key:
                key = key.replace('\\t', '\t')
            key_map[key] = ''
    return find_flag


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='帮助信息')
    parser.add_argument('--work', type=str, default='', help='代码目录')
    parser.add_argument('--others', type=str, default='', help='其他工程目录（用,分隔）')
    parser.add_argument('--output', type=str, default='', help='配置文件输出目录')
    parser.add_argument('--debug', type=int, default=1, help='调试标识')

    args = parser.parse_args()

    debug = args.debug
    if debug:
        work_path = 'C:/work/HLXXZ_RPG/HLXX_Client/RPG_Cocos/assets/script/s/m/lljh'
        others = ''
        other_paths = others.split(',')
        output_path = './test/lan'
    else:
        if not args.work:
            print('[ERROR]请指定项目代码目录')
            sys.exit()
        work_path = args.work
        others = args.others
        if others:
            other_paths = others.split(',')
        else:
            other_paths = None
        if not args.output:
            print('[ERROR]请指定配置输出目录')
            sys.exit()
        output_path = args.output

    run(work_path, output_path, other_paths)

    if debug:
        wb = Workbook()
        sheet = wb.active
        sheet['A1'] = 'key'
        sheet['B1'] = 'value'
        # 设置单元格宽度
        sheet.column_dimensions['A'].width = 50
        sheet.column_dimensions['B'].width = 50

        row = 1
        for k in key_map:
            row += 1
            sheet['A' + str(row)] = k
            sheet['B' + str(row)] = key_map[k]

        path_excel = Path('./excel/test.xlsx')
        path_excel.parent.mkdir(parents=True, exist_ok=True)
        wb.save(path_excel)
