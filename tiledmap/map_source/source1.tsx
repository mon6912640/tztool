<?xml version="1.0" encoding="UTF-8"?>
<tileset name="source1" tilewidth="60" tileheight="60" tilecount="8" columns="1">
 <image source="source1.png" width="64" height="512"/>
 <tile id="0">
  <properties>
   <property name="gType" type="int" value="5"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="gType" type="int" value="3"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="gType" type="int" value="4"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="gType" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="gType" type="int" value="1"/>
  </properties>
 </tile>
</tileset>
