import base64


def compress_uuid(uid):
    """
    uuid压缩，将uuid转换为__type__字符串
    :param uid:
    :return:
    """
    head = uid[:5]
    body = uid[5:]
    body = body.replace('-', '') + 'f'
    int_arr = []
    for x in range(len(body) - 1):
        if x % 2 == 0:
            int_arr.append(int(body[x:x + 2], 16))
    return head + str(base64.b64encode(bytes(int_arr)), 'utf-8')[:-2]


type_str = compress_uuid('c25ac5f1-272e-4d47-8e24-2ee6b8868e72')
print(type_str)

type_str2 = compress_uuid('dea9b589-f902-458f-a029-355c3385b6f9')
print(type_str2)
