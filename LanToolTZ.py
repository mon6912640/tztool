import argparse
import json
import re
import sys
from pathlib import Path
from typing import List

import cchardet as chardet
from tinydb import TinyDB, Query

# 上次的keymap
last_key_map = {}
# 当前的keymap
key_map = {}
# 新增的keymap
new_key_map = {}
# 匹配到的数量
find_cnt = 0

# 版本模式 0输出单个lan_cn.json 1输出带有版本号的json
ver_mode = 0

db: TinyDB
vid = 0


class VoFind:
    file_path: Path = None
    encoding = ''
    start_index = 0
    end_index = 0
    # 存储匹配到的字符串
    find_str = ''
    # 0 Lan.str  1 Lan.rep
    type_str = ''
    is_special = False

    # 引号
    quotation = None

    out_str = ''

    # 流水id
    uid = 0

    def __init__(self):
        global find_cnt
        find_cnt += 1
        self.uid = find_cnt

    def get_key(self):
        key = ''
        strip_str = self.find_str.lstrip().rstrip()  # 去除左右两边的空白字符
        reg_str = '{0}(.+?)(?<!\\\){0}'.format(self.quotation)
        # print('reg_str={0}'.format(reg_str))
        rec = re.compile(r'' + reg_str + '')  # 根据引号不同动态编译正则表达式
        # objtest = rec.search(strip_str)
        # if objtest:
        #     print('objtest.group(1)={0}'.format(objtest.group(1)))
        if self.out_str:  # 先处理替换修改过的，这里已经变成rep的形式
            # obj = rec.search(self.out_str)
            obj = re.search(r'\"(.+?)(?<!\\)\",', self.out_str)  # 这里不用原来的引号来匹配，需要匹配双引号
        elif self.type_str == 'rep':
            obj = rec.search(strip_str)
            # obj = re.search(r'[\'\"`](.+)[\'\"`],', strip_str)
        elif self.type_str == 'str':
            obj = rec.search(strip_str)
            # obj = re.search(r'[\'\"`](.+)[\'\"`]', strip_str)
        elif self.type_str == 'gLangVal':
            obj = rec.search(strip_str)
        else:
            obj = None
        if obj:
            key = obj.group(1)
            if '\\n' in key:
                key = key.replace('\\n', '\n')
            if '\\t' in key:
                key = key.replace('\\t', '\t')
            key_map[key] = ''
            if key not in last_key_map:  # 不在上次的keymap中则放到新增的keymap
                new_key_map[key] = ''
            # key_map[key] = self.encoding + ' ||| ' + str(self.file_path)
        # print('-------- strip_str={0}, key={1}'.format(strip_str, key))


class VoTs:
    url = ''
    file_path: Path = None
    code_str = ''
    has_special = False
    encoding = ''
    list_find: List[VoFind] = None

    def __init__(self):
        self.list_find = []


def get_encoding(p_file):
    """
    获取文件编码
    :param p_file:
    :return:
    """
    with open(p_file, 'rb') as f:
        msg = f.read()
        result = chardet.detect(msg)['encoding']
        return result


def check_encoding_support_chinese(p_encoding):
    """
    检查编码是否支持中文
    :param p_encoding:
    :return:
    """
    if p_encoding == 'GB2312':
        return True
    if p_encoding == 'GBK':
        return True
    if p_encoding == 'GB18030':
        return True
    if p_encoding == 'UTF-8':
        return True
    return False


def run(p_work, p_output, p_others=None):
    list_file = []
    if p_others:
        for v in p_others:
            psrc = Path(v)
            if psrc.exists():
                list_file.extend(psrc.rglob('*.ts'))
    path_src = Path(p_work)
    list_file.extend(path_src.rglob('*.ts'))
    list_file.sort()
    ts_count = len(list_file)
    print('共有{0}个ts文件'.format(ts_count))

    # 含有Lan函数的文件数量
    has_lan_ts_cnt = 0

    encoding_set = set()
    modify_ts_list = []  # 需要把str包含${x}写法的替换成rep的ts文件列表
    for v in list_file:
        if v.name == 'Lang.ts':
            continue
        if '.d' in v.stem:  # 过滤掉.d.ts文件
            continue
        cur_encoding = get_encoding(str(v))
        read_encoding = cur_encoding
        encoding_warning = False
        if not check_encoding_support_chinese(cur_encoding):
            # 由于有些文件是UTF-8编码，但是cchardet检测出来的是ISO-8859-7，所以这里强行使用UTF-8编码读取
            # 因为库是使用字符频率分析算法来检测编码的，所以有可能会出现错误的情况，遇到特定的中文字符组合也会导致检测错误
            # 当遇到不支持中文的编码时，强行使用UTF-8编码读取
            encoding_warning = True
            read_encoding = 'UTF-8'
        try:
            file_str = v.read_text(encoding=read_encoding)
        except Exception as e:
            print('[ERROR] 读取文件编码出错：{0}'.format(v))
            continue
        result = find_lan_func(file_str, 0)
        end_index = result[0]
        if end_index < 0:
            continue
        encoding_set.add(cur_encoding)
        if encoding_warning:
            print('[warning] 文件编码不支持中文，请检查提取结果是否乱码，路径：{0}'.format(v))
        vots = VoTs()
        vots.encoding = cur_encoding
        vots.url = str(v)
        vots.file_path = v
        vots.code_str = file_str
        len_str = len(file_str)
        pos = 0
        while end_index > -1:
            # print('+++++++++', v, file_str[end_index], end_index)
            quotation = result[3]  # 引号字符串
            bk = False

            for i in range(end_index, len_str):
                char = file_str[i]
                # print(char)
                if char == '(' and bk is False:
                    bk = True
                elif char == ')' and bk is True:
                    bk = False
                elif char == ')' and bk is False:
                    # print('+++++++++++++++')
                    vof = VoFind()
                    vof.encoding = cur_encoding
                    vof.file_path = v
                    vof.find_str = file_str[end_index:i]
                    vof.type_str = result[1]
                    vof.start_index = end_index
                    vof.end_index = i
                    vof.quotation = quotation
                    vots.list_find.append(vof)
                    pos = i

                    # print('==== find_str={0}'.format(vof.find_str))

                    inner_find_result = find_lan_func(vof.find_str, 0)
                    if inner_find_result[0] > -1:  # Lan函数内还有嵌套的Lan函数
                        # print('嵌套===================', str(v))
                        # print(vof.find_str)
                        pos = end_index + inner_find_result[2]  # inner_find_result[2]是Lan函数内的Lan函数的起始位置

                    # 把 ${xx} 替换成 &x& + 参数的形式
                    if vof.quotation == '`' and '${' in vof.find_str:
                        print('特殊处理', v)
                        vof.is_special = True
                        vots.has_special = True
                        params = []
                        len_elements = 0  # 原来有&x&的个数

                        def rpl_func(ma):
                            s2 = ma.group(2)
                            params.append(s2)
                            return '&' + str((len(params) - 1) + len_elements) + '&'  # 替换成&0&、&1&、&2&等形式

                        strip_str = vof.find_str.lstrip().rstrip()  # 去除左右两边的空白字符
                        element_list = re.findall(r'&\d+&', strip_str)
                        if len(element_list) > 0:
                            element_set = set(element_list)
                            len_elements = len(element_set)
                        out_str = re.sub(r'(\$){([^}]+)}', rpl_func, strip_str, flags=re.M)
                        out_str = out_str + ', ' + ', '.join(params)
                        out_str = out_str.replace('`', '"')
                        print('out_str', out_str)
                        vof.out_str = out_str

                    vof.get_key()
                    break

            # print('pos', pos)
            result = find_lan_func(file_str, pos)
            end_index = result[0]
            # print('end_index', end_index)
        if vots.has_special:
            modify_ts_list.append(vots)

        if len(vots.list_find) > 0:
            has_lan_ts_cnt += 1

    print(encoding_set)
    print('    共有{0}个ts文件含有Lang函数'.format(has_lan_ts_cnt))
    for vts in modify_ts_list:
        for vfind in vts.list_find:
            if vfind.is_special:
                # 把 gLangVal(`xxxx${xxxx}`替换成gLangVal("xxxx{0}", xxxx
                old_str = vfind.type_str + '(' + vfind.find_str
                new_str = 'gLangVal(' + vfind.out_str
                vts.code_str = vts.code_str.replace(old_str, new_str)
        vts.file_path.write_text(vts.code_str, encoding=vts.encoding)

    global vid
    out_name = 'lan_cn.json'
    new_cnt = 0
    generate_add = False
    if ver_mode > 0:
        new_cnt = len(new_key_map)  # 新增key的数量
        if new_cnt > 0:
            # 有新增key
            vid += 1
            generate_add = True
        else:
            # 无新增key
            pass
        out_name = f'lan_cn_v{vid}.json'

    json_str = json.dumps(key_map, ensure_ascii=False, indent=4)
    path_json = Path(p_output, out_name)
    path_json.parent.mkdir(parents=True, exist_ok=True)
    path_json.write_text(json_str, encoding='utf-8')
    print('...成功生成语言配置 {0}'.format(path_json))

    if generate_add:  # 生成差异文件
        add_str = json.dumps(new_key_map, ensure_ascii=False, indent=4)
        path_add = Path(p_output, f'add_lan_v{vid - 1}-v{vid}.json')
        path_add.parent.mkdir(parents=True, exist_ok=True)
        path_add.write_text(add_str, encoding='utf-8')
        print(f'...生成版本差异配置 {str(path_add)}')

    if ver_mode > 0:
        db_find = db.search(ver_query.vid == vid)
        if len(db_find) == 0:
            db.insert({'vid': vid})

    print(ver_mode)


def find_lan_func(p_str, p_pos):
    """
    查找Lan.gLangVal(
    :param p_str:
    :param p_pos:
    :return:
        0 end_index 匹配到的模式的结束位置（减去空白字符和引号的长度）,
        1 type_str 匹配到的是str还是rep,
        2 start_index 匹配到的模式的开始位置,
        3 quotation 匹配到的引号类型（反引号、单引号或双引号）,
        4 old_end_index 匹配到的模式的结束位置
    """
    pattern = re.compile(r'\WLang\.I\.(gLangVal)\((\s*)([`\'\"])')
    start_index = -1
    quotation = None
    old_end_index = -1
    end_index = -1
    type_str = ''
    obj = pattern.search(p_str, p_pos)
    if obj:
        span = obj.span()
        type_str = obj.group(1)
        whitespace = obj.group(2)
        quotation = obj.group(3)  # 引号
        len_whitespace = 0
        if whitespace:  # 有空白字符
            len_whitespace = len(whitespace)
        end_index = span[1] - 1 - len_whitespace  # 减去空白字符和引号的长度
        old_end_index = span[1]
        start_index = span[0]  # 增加起始位置
    return [end_index, type_str, start_index, quotation, old_end_index]


def diff_lan(p_dir, p_vid1, p_vid2):
    """
    生成两个版本的差异文件
    :param p_dir:版本文件所在目录
    :param p_vid1:版本号1
    :param p_vid2:版本号2
    :return:
    """
    path_dir = Path(p_dir)
    if not path_dir.exists():
        print('[ERROR] 目录不存在')
        return 1
    if p_vid1 == p_vid2:
        print('[ERROR] 两个版本号不能相同')
        return 1
    old_vid = 0
    new_vid = 0
    if p_vid1 < p_vid2:
        old_vid = p_vid1
        new_vid = p_vid2
    else:
        old_vid = p_vid2
        new_vid = p_vid1

    path_lan_old = path_dir.joinpath(f'lan_cn_v{old_vid}.json')
    path_lan_new = path_dir.joinpath(f'lan_cn_v{new_vid}.json')

    new_map = {}
    if not path_lan_old.exists():
        print('[ERROR] lan_cn_v{0}.json不存在'.format(old_vid))
        return 1
    if not path_lan_new.exists():
        print('[ERROR] lan_cn_v{0}.json不存在'.format(new_vid))
        return 1
    try:
        encoding_old = get_encoding(str(path_lan_old))
        file_str_old = path_lan_old.read_text(encoding=encoding_old)
        obj_map_old = json.loads(file_str_old)

        encoding_new = get_encoding(str(path_lan_new))
        file_str_new = path_lan_new.read_text(encoding=encoding_new)
        obj_map_new = json.loads(file_str_new)
    except:
        print('[ERROR] 读取文件出错')
        return 1
    else:
        for k in obj_map_new:
            if k not in obj_map_old:
                new_map[k] = obj_map_new[k]
        if len(new_map) > 0:
            json_str = json.dumps(new_map, ensure_ascii=False, indent=4)
            path_json = Path(p_dir, f'add_lan_v{old_vid}-v{new_vid}.json')
            path_json.parent.mkdir(parents=True, exist_ok=True)
            path_json.write_text(json_str, encoding='utf-8')
            print(f'...生成版本差异配置 {str(path_json)}')
    return 0


class VoLabel:
    file_path: Path = None
    text = ''
    has_lancom = False

    def __init__(self):
        self.chain = []

    def find_chain(self, p_node, p_prefab):
        # 递归查找节点的链路
        self.chain.insert(0, p_node['_name'])  # 把节点名插入到链路头部
        parent_obj = p_node['_parent']
        while parent_obj:
            p_node = p_prefab[parent_obj['__id__']]
            self.chain.insert(0, p_node['_name'])  # 把节点名插入到链路头部
            parent_obj = p_node['_parent']


def find_all_label_from_prefab(p_dir, p_lancom_type):
    path_dir = Path(p_dir)
    if not path_dir.exists():
        print('[ERROR] 目录不存在')
        return 1
    labels = []
    # 遍历文件夹内所有prefab文件
    list_file = sorted(path_dir.rglob('*.prefab'))
    prefab_cnt = len(list_file)
    for p in list_file:
        try:
            cur_encoding = get_encoding(str(p))
            prefab_str = p.read_text(encoding=cur_encoding)
            # prefab文件其实就是json格式的，所以直接用json解析即可
            # 参考：https://forum.cocos.org/t/prefab-json--components---id--/80854
            obj_prefab = json.loads(prefab_str)  # 解析出来这个json对象其实就是一个数组结构
            for index, item in enumerate(obj_prefab):
                if '__type__' in item and item['__type__'] == 'cc.Label':
                    # 找到label组件
                    if '_string' not in item or not item['_string']:
                        # 跳过string为空的label
                        continue
                    vo_label = VoLabel()
                    vo_label.file_path = p
                    vo_label.text = item['_string']
                    labels.append(vo_label)
                    # 找到其挂载的节点
                    if 'node' in item:
                        node_id = item['node']['__id__']  # label组件挂载的节点id
                        parent_node = obj_prefab[node_id]  # 找到父节点
                        vo_label.find_chain(parent_node, obj_prefab)  # 构建节点链路
                        components = parent_node['_components']  # 父节点的组件列表
                        for com in components:
                            com_id = com['__id__']
                            if com_id == index:  # 跳过当前label组件（组件id等于数组下标）
                                continue
                            item_com = obj_prefab[com_id]
                            if '__type__' in item_com and item_com['__type__'] == p_lancom_type:
                                # 找到LanCom组件
                                vo_label.has_lancom = True
                                break
        except:
            continue
    print(f'共有{prefab_cnt}个prefab文件')
    print(f'找到{len(labels)}个string不为空的label组件')
    # print(f'其中有{sum(1 for l in labels if l.has_lancom)}个label组件挂载了{p_lancom_type}组件')
    for lb in labels:
        if lb.has_lancom:
            print(f'{lb.chain}, {lb.text}')
    return 0


def find_piaozi_with_com(p_dir, p_com_type):
    """
    在一个目录中查找具有特定组件的预制文件，并且该组件有改变position的行为
    :param p_dir:
    :param p_com_type:
    :return:
    """
    path_dir = Path(p_dir)
    if not path_dir.exists():
        print('[ERROR] 目录不存在')
        return 1
    anim_map = {}
    # 遍历所有 anim.meta 文件
    meta_list = sorted(path_dir.rglob('*.anim.meta'))
    for m in meta_list:
        try:
            cur_encoding = get_encoding(str(m))
            meta_str = m.read_text(encoding=cur_encoding)
            obj_meta = json.loads(meta_str)
            if 'uuid' not in obj_meta:
                continue
            # print(m.stem)
            anim_uuid = obj_meta['uuid']  # anim文件的uuid
            anim_map[anim_uuid] = m
        except:
            continue

    list_file = sorted(path_dir.rglob('*.prefab'))
    prefab_cnt = len(list_file)
    has_com_cnt = 0
    for p in list_file:
        try:
            cur_encoding = get_encoding(str(p))
            prefab_str = p.read_text(encoding=cur_encoding)
            obj_prefab = json.loads(prefab_str)
            for index, item in enumerate(obj_prefab):
                if '__type__' in item and item['__type__'] == p_com_type:
                    if '_defaultClip' not in item or not item['_defaultClip']:
                        continue
                    # print(f'clips长度：{clips_len}')
                    anim_uuid = item['_defaultClip']['__uuid__']  # anim文件的uuid
                    if anim_uuid not in anim_map:
                        continue
                    path_anim_meta = anim_map[anim_uuid]
                    path_anim = path_anim_meta.parent / path_anim_meta.stem
                    if not path_anim.exists():
                        continue
                    anim_str = path_anim.read_text(encoding=get_encoding(str(path_anim)))
                    if 'position' in anim_str:
                        print(p)
                        has_com_cnt += 1
                        # print(f'anim文件中有position')
                        break
        except:
            continue
    print('============================')
    print('============================')
    print(f'共有{prefab_cnt}个prefab文件')
    print(f'找到{has_com_cnt}个挂载了{p_com_type}组件且有改变position的prefab')
    return 0


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='帮助信息')
    parser.add_argument('--work', type=str, default='', help='代码目录')
    parser.add_argument('--others', type=str, default='', help='其他工程目录（用,分隔）')
    parser.add_argument('--output', type=str, default='', help='配置文件输出目录')
    parser.add_argument('--ver', type=int, default=1,
                        help='版本模式 0单独输出lan_cn.json文件，1版本模式：输出带有版本号的json文件')
    parser.add_argument('--debug', type=int, default=1, help='调试标识')
    parser.add_argument('--diff', type=str, default='', help='差异模式 用法：--diff 1,2')

    args = parser.parse_args()

    if args.diff:
        debug = args.debug
        if debug:
            output_path = './test/lan'
        else:
            output_path = args.output
        # 差异模式
        if ',' not in args.diff:
            print('[ERROR] 请指定两个版本号')
            sys.exit()
        if not output_path:
            print('[ERROR] 请指定配置输出目录')
            sys.exit()
        path_out = Path(output_path)
        if not path_out.exists():
            print('[ERROR] 输出路径不存在')
            sys.exit()
        if path_out.is_file():
            print('[ERROR] 输出路径不能是文件')
            sys.exit()
        vid1 = int(args.diff.split(',')[0])
        vid2 = int(args.diff.split(',')[1])
        diff_lan(str(path_out), vid1, vid2)

    else:
        # 提取模式
        ver_mode = args.ver

        debug = args.debug
        if debug:
            # work_path = 'C:/work/HLXXZ_RPG/HLXX_Client/RPG_Cocos/assets/script/s/m/lljh'
            work_path = 'C:/work_mine/cocos/SpineTest/assets/script'
            # work_path = 'C:/work/HLXXZ_RPG/HLXX_Client/RPG_Cocos/assets/script'
            # work_path = 'C:/work/HLXXZ_RPG/HLXX_Client/RPG_Cocos/assets/script/s/sg/zc_reskin'
            others = ''
            other_paths = others.split(',')
            output_path = './test/lan'
        else:
            if not args.work:
                print('[ERROR]请指定项目代码目录')
                sys.exit()
            work_path = args.work
            others = args.others
            if others:
                other_paths = others.split(',')
            else:
                other_paths = None
            if not args.output:
                print('[ERROR]请指定配置输出目录')
                sys.exit()
            output_path = args.output

        path_out = Path(output_path)
        if path_out.is_file():
            print('[ERROR] 输出路径不能是文件')
            sys.exit()
        path_out.mkdir(parents=True, exist_ok=True)

        # path_work = Path(work_path)
        #
        # print(str(path_work))
        # crc = binascii.crc32(str(path_work).encode())
        # print(crc)

        print(path_out.joinpath('db.json'))
        db = TinyDB(path_out.joinpath('db.json'))  # 读取输出目录的数据库
        ver_query = Query()
        all_list = db.all()
        # print(all_list)
        obj_map = None
        if len(all_list) == 0:
            pass
        else:
            for v in all_list[::-1]:
                versionid = v['vid']
                path_lan = path_out.joinpath(f'lan_cn_v{versionid}.json')
                if path_lan.exists():
                    try:
                        encoding = get_encoding(str(path_lan))
                        file_str = path_lan.read_text(encoding=encoding)
                        obj_map = json.loads(file_str)
                    except:
                        continue
                    else:
                        last_key_map = obj_map
                        vid = versionid
                        break

        if last_key_map:
            # 把上次的keymap全量赋予给当前的keymap
            for k in last_key_map:
                key_map[k] = last_key_map[k]

        run(work_path, output_path, other_paths)
