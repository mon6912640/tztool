"""
检查标题字体是否有缺失字符的脚本工具
"""
import json
from pathlib import Path

cahr_set1 = set()
char_set2 = set()


def check_char(p_char: str, p_code_set, p_title_type):
    """
    检查字符是否在标题字体中
    :param p_char:
    :param p_code_set:
    :param p_title_type:
    :return:
    """
    # encoded_char = p_char.encode('UTF-16BE')
    # decimal_val = int.from_bytes(encoded_char, byteorder='big')
    # print(f'"{p_char}"的UTF-16BE十进制值为{decimal_val}')

    if p_title_type == 1:
        char_set = cahr_set1
    else:
        char_set = char_set2

    if p_char in char_set:
        return

    char_set.add(p_char)

    decimal_val = ord(p_char)
    # print(f'"{p_char}"的unicode十进制值为{decimal_val}')
    if decimal_val not in p_code_set:
        if p_title_type == 1:
            local_des = '一级弹窗标题'
        else:
            local_des = '二级弹窗标题'
        print(f'{local_des} 缺失字符 "{p_char}" ，unicode十进制值为= {decimal_val}')


def convert_to_int(s):
    """
    将字符串转换为整数，如果字符串不能转换，则返回-1
    :param s:
    :return:
    """
    try:
        return int(s)
    except ValueError:
        return -1


if __name__ == '__main__':

    source = Path('./titlefont.json')
    source_str = source.read_text(encoding='UTF-8')
    obj_map = json.loads(source_str)

    if obj_map['res_branch']:
        res_branch = 'HLXX_Res'+'_'+obj_map['res_branch']
    else:
        res_branch = 'HLXX_Res'

    url1 = f'C:/work/HLXXZ_RPG/HLXX_Res/{res_branch}/f/singleFont/title1'
    code_set1 = set()
    img_dir1 = Path(url1)
    list_file1 = img_dir1.rglob('*.png')
    for f in list_file1:
        # print(f.stem)
        val = convert_to_int(f.stem)
        if val != -1:
            code_set1.add(val)
        else:
            print(f'title1 文件名不是数字，文件名为：{f.stem}.png')
    # print(code_set1)

    url2 = f'C:/work/HLXXZ_RPG/HLXX_Res/{res_branch}/f/singleFont/title2'
    code_set2 = set()
    img_dir2 = Path(url2)
    list_file2 = img_dir2.rglob('*.png')
    for f in list_file2:
        # print(f.stem)
        val = convert_to_int(f.stem)
        if val != -1:
            code_set2.add(val)
        else:
            print(f'title2 文件名不是数字，文件名为：{f.stem}.png')
    # print(code_set2)

    for k in obj_map:
        # v是一个字符串
        if k == "1" or k == "2":
            title_type = int(k)
            if title_type == 1:
                code_set = code_set1
            else:
                code_set = code_set2
            str_list = obj_map[k]
            for v in str_list:
                chars = list(v)
                for c in chars:
                    check_char(c, code_set, title_type)

