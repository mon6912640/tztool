from pathlib import Path
import cchardet as chardet
import base64
import json


class VoTs:
    file_path: Path = None
    encoding = ''
    uuid = ''
    type_str = ''


def compress_uuid(uid):
    """
    uuid压缩，将uuid转换为__type__字符串
    :param uid:
    :return:
    """
    head = uid[:5]
    body = uid[5:]
    body = body.replace('-', '') + 'f'
    int_arr = []
    for x in range(len(body) - 1):
        if x % 2 == 0:
            int_arr.append(int(body[x:x + 2], 16))
    return head + str(base64.b64encode(bytes(int_arr)), 'utf-8')[:-2]


def get_encoding(p_file):
    """
    获取文件编码
    :param p_file:
    :return:
    """
    with open(p_file, 'rb') as f:
        msg = f.read()
        result = chardet.detect(msg)['encoding']
        return result


work_dir = 'C:/work/HLXXZ_RPG/HLXX_Client/RPG_Cocos/assets/script'
path_work = Path(work_dir)
prefab_dir = 'C:/work/HLXXZ_RPG/HLXX_Client/RPG_Cocos/assets/resources/p'
path_prefab = Path(prefab_dir)
resource_dir = 'C:/work/HLXXZ_RPG/HLXX_Client/RPG_Cocos/assets/resources'

ts_vo_list = []

list_prefab = sorted(path_prefab.rglob('*.prefab'))
prefab_cnt = 0
for p in list_prefab:
    cur_endcoding = get_encoding(str(p))
    prefab_str = p.read_text(encoding=cur_endcoding)

list_file = sorted(path_work.rglob('*.ts'))
ts_cnt = 0
for ts in list_file:
    cur_endcoding = get_encoding(str(ts))
    ts_str = ts.read_text(encoding=cur_endcoding)
    # 检查是否含有“ extends WinBig”字符串
    if ' extends WinBig' in ts_str:
        ts_cnt += 1
        # print(ts.stem)
        # 载入同目录下的meta文件
        meta_file = ts.parent / (ts.name + '.meta')
        # 读取meta文件，json反序列化，取得uuid
        meta_str = meta_file.read_text(encoding='UTF-8')
        obj_meta = json.loads(meta_str)
        ts_uuid = obj_meta['uuid']
        # print(f'uuid = {ts_uuid}')
        ts_type = compress_uuid(ts_uuid)
        # print(f'uuid->__type__ = {ts_type}')
        ts_vo = VoTs()
        ts_vo.file_path = ts
        ts_vo.encoding = cur_endcoding
        ts_vo.uuid = ts_uuid
        ts_vo.type_str = ts_type
        ts_vo_list.append(ts_vo)

str_set = set()
list_prefab = sorted(path_prefab.rglob('*.prefab'))
prefab_cnt = 0
for p in list_prefab:
    cur_endcoding = get_encoding(str(p))
    prefab_str = p.read_text(encoding=cur_endcoding)
    for ts_vo in ts_vo_list:
        if ts_vo.type_str in prefab_str:
            prefab_cnt += 1
            # print(p.stem)
            # 计算相对路径 prefab文件相对于resource目录的路径，用“/”分隔符
            prefab_rel = p.relative_to(resource_dir)
            path_url = str(prefab_rel.as_posix())
            # 去掉后缀 .prefab
            path_url = path_url[:-7]
            out_str = '"' + path_url + '.prefab"' + ','
            # print(out_str)
            str_set.add(out_str)
            # print('    ', ts_vo.file_path.name)
            break

source_list = [
    'p/m/answer/Answer.prefab',
    'p/m/answer/Answer_FZP.prefab',
    'p/m/bag/Bag.prefab',
    'p/m/bag/TabBag.prefab',
    'p/m/mail/Mail.prefab',
    'p/m/bag/Stick.prefab',
    'p/m/bag/TabRse.prefab',
    'p/m/bag/TabTunJin.prefab',
    'p/m/bag/EqpSmt.prefab',
    'p/m/boss/BossP.prefab',
    'p/m/boss/CSBoss.prefab',
    'p/m/boss/exmChl/ExmChl.prefab',
    'p/m/boss/ttBoss/TTBoss.prefab',
    'p/m/boss/rnkCpt/RnkCpt.prefab',
    'p/m/boss/ItmDrp.prefab',
    'p/m/ccyj/CcyjEnter.prefab',
    'p/m/ccyj/CcyjP.prefab',
    'p/m/grade/CirFnd.prefab',
    'p/m/grade/CirTFd.prefab',
    'p/m/claWar/ClaPan.prefab',
    'p/m/claWar/ClWrDt.prefab',
    'p/m/claWar/ClWrLt.prefab',
    'p/m/clhClt/ClhClt.prefab',
    'p/m/clhClt/ClhXS.prefab',
    'p/m/clhClt/ClhGL.prefab',
    'p/m/clhClt/ClhFX.prefab',
    'p/m/clhClt/ClhXS.prefab',
    'p/m/com/PetBSt.prefab',
    'p/m/com/WarBSt.prefab',
    'p/m/temPrt/TemPrt.prefab',
    'p/m/comTmSet/ComTmSet.prefab',
    'p/m/daily/Daily.prefab',
    'p/m/daily/practi/PracUI.prefab',
    'p/m/daily/rsg/RSGUI.prefab',
    'p/m/daily/dlyAct/DlyAct.prefab',
    'p/m/newAct/fndBao/xldx/XldxUi.prefab',
    'p/m/daily/ddp/DDP.prefab',
    'p/m/actNte/ActNtP.prefab',
    'p/m/funOpn/FunOpen.prefab',
    'p/m/daily/dlyAct/shenNv/SelShN.prefab',
    'p/m/dragon/Dragon.prefab',
    'p/m/dragon/DrgnPY.prefab',
    'p/m/dragon/DrgGMn.prefab',
    'p/m/dragon/DrgSta.prefab',
    'p/m/dragon/DrgShadow.prefab',
    'p/m/dragon/DrgBag.prefab',
    'p/m/dragon/DragFJ.prefab',
    'p/m/dragon/DragOp.prefab',
    'p/m/dragon/DrgGUp.prefab',
    'p/m/dragon/DrgWsh.prefab',
    'p/m/xiaDan/XiaDanWrap.prefab',
    'p/m/xiaDan/XiaDan.prefab',
    'p/m/xiaDan/GodDan.prefab',
    'p/m/eqpDev/EqpDev.prefab',
    'p/m/eqpDev/EqpSth.prefab',
    'p/m/eqpDev/EqpUpS.prefab',
    'p/m/eqpDev/EqpRfn.prefab',
    'p/m/eqpDev/StarUI.prefab',
    'p/m/eqpDev/EqpSto.prefab',
    'p/m/eqpDev/EqpJL.prefab',
    'p/m/eqpDev/EqpCSl.prefab',
    'p/m/eqpDev/EqpDGod.prefab',
    'p/m/eqpDev/EqpGForg.prefab',
    'p/m/eqpDev/EqpDaZ.prefab',
    'p/m/eqpDev/GodRne.prefab',
    'p/m/exChag/GoldEg.prefab',
    'p/m/fairy/FairyP.prefab',
    'p/m/fairy/FayUp.prefab',
    'p/m/fairy/FaySta.prefab',
    'p/m/fairy/FyClMn.prefab',
    'p/m/mars/Mars.prefab',
    'p/m/grade/UpDegr.prefab',
    'p/m/fairy/FyCxBI.prefab',
    'p/m/jingMai/JingMai.prefab',
]

for source in source_list:
    str_set.add('"' + source + '",')

out_list = []
for s in str_set:
    out_list.append(s)
out_list.sort()
for o in out_list:
    print(o)

# print(f'共有{ts_cnt}个ts文件继承了WinBig')
